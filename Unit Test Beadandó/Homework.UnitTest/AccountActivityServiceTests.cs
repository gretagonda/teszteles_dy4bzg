﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountActivityServiceTests
    {
        private Mock<IAction> _mockAction;
        //private AccountActivityService _accountActivityService;
        private FakeAccountRepository _fakeAccountRepository;
        private Account _accountNone;
        private Account _account1;
        private Account _account10;
        private Account _account19;
        private Account _account20;
        private Account _account30;
        private Account _account39;
        private Account _account40;
        private Account _account100;

        //private Mock<IAccountRepository> _mockAccountRepository;
        private Mock<IAccountActivityService> _mockAccountActivityService;

        [SetUp]
        public void SetUp()
        {
            _mockAction = new Mock<IAction>();
            _fakeAccountRepository = new FakeAccountRepository();
            _accountNone = new Account(0);
            _accountNone.Activate();
            _accountNone.Register();
            _fakeAccountRepository.Add(_accountNone);
            _account1 = new Account(1);
            _account1.Activate();
            _account1.Register();
            ActionsSetUp(1, _account1);
            _fakeAccountRepository.Add(_account1);
            _account10 = new Account(10);
            _account10.Activate();
            _account10.Register();
            ActionsSetUp(10, _account10);
            _fakeAccountRepository.Add(_account10);
            _account19 = new Account(19);
            _account19.Activate();
            _account19.Register();
            ActionsSetUp(19, _account19);
            _fakeAccountRepository.Add(_account19);
            _account20 = new Account(20);
            _account20.Activate();
            _account20.Register();
            ActionsSetUp(20, _account20);
            _fakeAccountRepository.Add(_account20);
            _account30 = new Account(30);
            _account30.Activate();
            _account30.Register();
            ActionsSetUp(30, _account30);
            _fakeAccountRepository.Add(_account30);
            _account39 = new Account(39);
            _account39.Activate();
            _account39.Register();
            ActionsSetUp(39, _account39);
            _fakeAccountRepository.Add(_account39);
            _account40 = new Account(40);
            _account40.Activate();
            _account40.Register();
            ActionsSetUp(40, _account40);
            _fakeAccountRepository.Add(_account40);
            _account100 = new Account(100);
            _account100.Activate();
            _account100.Register();
            ActionsSetUp(100, _account100);
            _fakeAccountRepository.Add(_account100);
           // _accountActivityService = new AccountActivityService(_fakeAccountRepository);

           // _mockAccountRepository = new Mock<IAccountRepository>();
            _mockAccountActivityService = new Mock<IAccountActivityService>(_fakeAccountRepository);
        }

        [Test]
        public void AccountIsNull_GetActivity_ThrowsAccountNotExistException()
        {
            _mockAccountActivityService.Setup(provider => provider.GetActivity(It.IsAny<int>())).Throws<AccountNotExistsException>();
            Assert.That(() => _accountActivityService.GetActivity(-1), Throws.TypeOf<AccountNotExistsException>());
        }

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsZero_GetActivity_ActivityLevelIsNone()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.None;

        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(0)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsOne_GetActivity_ActivityLevelIsLow()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.Low;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(1)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsTen_GetActivity_ActivityLevelIsLow()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.Low;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(10)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsNineteen_GetActivity_ActivityLevelIsLow()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.Low;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(19)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsTwenty_GetActivity_ActivityLevelIsMedium()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.Medium;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(20)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsThirty_GetActivity_ActivityLevelIsMedium()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.Medium;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(30)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsThirtynine_GetActivity_ActivityLevelIsMedium()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.Medium;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(39)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsFourty_GetActivity_ActivityLevelIsHigh()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.High;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(40)));
        //}

        //[Test]
        //public void ActionsSuccsessfullyPerformedIsOnehundred_GetActivity_ActivityLevelIsHigh()
        //{
        //    ActivityLevel expectedActivity = ActivityLevel.High;


        //    Assert.That(expectedActivity, Is.EqualTo(_accountActivityService.GetActivity(100)));
        //}



        //JAVITAS 1.
        // GetActivity - külön metodusok helyett testcase:

        [TestCase(ActivityLevel.None, 0)]
        [TestCase(ActivityLevel.Low,1)]
        [TestCase(ActivityLevel.Low, 10)]
        [TestCase(ActivityLevel.Low,19)]
        [TestCase(ActivityLevel.Medium,20)]
        [TestCase(ActivityLevel.Medium,30)]
        [TestCase(ActivityLevel.Medium,39)]
        [TestCase(ActivityLevel.High,40)]
        [TestCase(ActivityLevel.High,100)]
        public void ActionSuccessfullyPerformed_GetActivity_ActivityLevel(ActivityLevel aLevel, int accountId)
        {
            ActivityLevel expectedActivityLevel = aLevel;
            ActivityLevel resultActivityLevel = _accountActivityService.GetActivity(accountId);
            Assert.That(expectedActivityLevel, Is.EqualTo(resultActivityLevel));
        }



        //JAVITAS 2.
        // GetAmountForActivity - külön metodusok helyett testcase:

        //[Test]
        //public void SumLowActivityIsAsked_GetAmountForActivity_SumIsThree()
        //{
        //    ActivityLevel askedLevel = ActivityLevel.Low;
        //    int expectedSum = 3;
        //    int resultSum = _accountActivityService.GetAmountForActivity(askedLevel);
        //    Assert.That(expectedSum, Is.EqualTo(resultSum));
        //}

        //[Test]
        //public void SumMediumActivityIsAsked_GetAmountForActivity_SumIsThree()
        //{
        //    ActivityLevel askedLevel = ActivityLevel.Medium;
        //    int expectedSum = 3;
        //    int resultSum = _accountActivityService.GetAmountForActivity(askedLevel);
        //    Assert.That(expectedSum, Is.EqualTo(resultSum));
        //}


        //[Test]
        //public void SumNoneActivityIsAsked_GetAmountForActivity_SumIsZero()
        //{
        //    ActivityLevel askedLevel = ActivityLevel.None;
        //    int expectedSum = 1;
        //    int resultSum = _accountActivityService.GetAmountForActivity(askedLevel);
        //    Assert.That(expectedSum, Is.EqualTo(resultSum));
        //}

        //[Test]
        //public void SumHighActivityIsAsked_GetAmountForActivity_SumIsTwo()
        //{
        //    ActivityLevel askedLevel = ActivityLevel.High;
        //    int expectedSum = 2;
        //    int resultSum = _accountActivityService.GetAmountForActivity(askedLevel);
        //    Assert.That(expectedSum, Is.EqualTo(resultSum));
        //}

        [TestCase(ActivityLevel.None,1)]
        [TestCase(ActivityLevel.Low,3)]
        [TestCase(ActivityLevel.Medium,3)]
        [TestCase(ActivityLevel.High,2)]
        public void SumActivityIsAsked_GetAmountForActivity_Sum(ActivityLevel askedActivityLevel, int expectedSum)
        {
            int resultSum = _accountActivityService.GetAmountForActivity(askedActivityLevel);
            Assert.That(expectedSum, Is.EqualTo(resultSum));
        }
        private void ActionsSetUp(int num, Account a)
        {
            _mockAction.Setup(action => action.Execute()).Returns(true);

            for (int i = 0; i < num; i++)
            {
                a.TakeAction(_mockAction.Object);
            }
        }
    }
}
