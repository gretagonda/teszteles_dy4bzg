﻿using Homework.ThirdParty;
using Moq;
using NUnit.Framework;
using System;

namespace Homework.UnitTests
{
    [TestFixture]
    public class AccountTests
    {
        private Mock<IAction> _mockAction;
        private Account _account;

        [SetUp]
        public void SetUp()
        {
            _mockAction = new Mock<IAction>();
            _account = new Account(1);
        }
        [Test]
        public void GivenValidAccount_Register_SuccessfullyRegistered()
        {

            _account.Register();

            Assert.That(_account.IsRegistered, Is.True);
        }

        [Test]
        public void GivenValidAccount_Activate_SuccessfullyConfirmed()
        {
            _account.Activate();

            Assert.That(_account.IsConfirmed, Is.True);
        }

        [Test]
        public void GivenValidAccount_TakeAction_ProperMethodIsCalled()
        {

            _account.Register();
            _account.Activate();
            _mockAction.Setup(action => action.Execute()).Returns(true);

            bool success = _account.TakeAction(_mockAction.Object);

            _mockAction.Verify(action => action.Execute());
            _mockAction.VerifyNoOtherCalls();

        }

        [Test]
        public void GivenRegisteredAndActivatedAccount_TakeAction_ReturnsTrueToSuccess()
        {

            _account.Register();
            _account.Activate();
            _mockAction.Setup(action => action.Execute()).Returns(true);

            bool success = _account.TakeAction(_mockAction.Object);

            Assert.That(success, Is.True);

        }


        [Test]
        public void GivenRegisteredButNotActivatedAccount_TakeAction_ReturnsTrueToSuccess()
        {
            _account.Register();
            _mockAction.Setup(action => action.Execute()).Returns(true);

            bool success = _account.TakeAction(_mockAction.Object);
            Assert.That(success, Is.True);

        }

        [Test]
        public void GivenNotRegisteredButActivatedAccount_TakeAction_ReturnsTrueToSuccess()
        {
            _account.Activate();
            _mockAction.Setup(action => action.Execute()).Returns(true);

            bool success = _account.TakeAction(_mockAction.Object);
            Assert.That(success, Is.True);

        }

        [Test]
        public void GivenActiveAccount_TakeAction_ActionsSuccessfullyPerformedIsIncremented()
        {
            _account.Activate();
            _mockAction.Setup(action => action.Execute()).Returns(true);

            int startedWith = _account.ActionsSuccessfullyPerformed;
            int expected = startedWith + 1;
            _account.TakeAction(_mockAction.Object);
            Assert.That(expected, Is.EqualTo(_account.ActionsSuccessfullyPerformed));
        }

        [Test]
        public void GivenInactiveAccount_TakeAction_ThrowsInactiveUserException()
        {

            _mockAction.Setup(action => action.Execute()).Returns(true);

            Assert.That(() => _account.TakeAction(_mockAction.Object), Throws.TypeOf<InactiveUserException>());
        }


    }
}
