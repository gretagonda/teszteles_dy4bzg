﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebdriverClass.WidgetsAtClass.BeadandoPropertySearchWidget;

namespace WebdriverClass.Models
{
    class PropertySearchModel
    {
        //public CountyOptions county;
        //public CategoryOptions category;
        public string city;
        public string minPrice;
        public string maxPrice;
        public string minArea;
        public string maxArea;
        public PropertySearchModel()
        {

        }

        public PropertySearchModel(string city, string minPrice, string maxPrice, string minArea, string maxArea)
        {

            this.city = city;
            this.minPrice = minPrice;
            this.maxPrice = maxPrice;
            this.minArea = minArea;
            this.maxArea = maxArea;
        }
    }
}
