﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebdriverClass.WidgetsAtClass.SearchWidget;

namespace WebdriverClass.Models
{
    class SearchModel
    {
        public string FromCity;
        public string ToCity;
        public string ViaCity;
        public Reductions Reduction;
        public SearchOptions SearchOption;

        public SearchModel()
        {
        }
        public SearchModel(string FromCity, string ToCity, string ViaCity, Reductions Reduction, SearchOptions SearchOption)
        {
            this.FromCity = FromCity;
            this.ToCity = ToCity;
            this.ViaCity = ViaCity;
            this.Reduction = Reduction;
            this.SearchOption = SearchOption;
        }
    }
}
