﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace WebdriverClass
{
    class WebdriverWaitTestAtClass : TestBase
    {
        [Test]
        public void WaitTitle()
        {
            Driver.Navigate().GoToUrl("http://www.google.com");
            Driver.Manage().Cookies.DeleteCookieNamed("CONSENT");
            var cookie = new Cookie("CONSENT", "YES+HU.hu+V12+BX");
            Driver.Manage().Cookies.AddCookie(cookie);
            Driver.Navigate().Refresh();

            IWebElement query = Driver.FindElement(By.Name("q"));
            query.SendKeys("Selenium");
            query.Submit();

            //create a WebDriverWait which waits until the page title starts with "Selenium"
            //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            //wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("gLFyf gsfi")));
            Assert.AreEqual("Selenium - Google-keresés", Driver.Title);
        }

        [Test]
        public void WaitKeyboard()
        {
           // ChromeOptions options = new ChromeOptions();
            //options.AddArguments("--lang=hu");
            //Driver = new ChromeDriver(options);

            Driver.Navigate().GoToUrl("http://www.google.hu");
            Driver.Manage().Cookies.DeleteAllCookies();
            var cookie = new Cookie("CONSENT", "YES+HU.hu+V12+BX");
            Driver.Manage().Cookies.AddCookie(cookie);
            Driver.Navigate().Refresh();

            Driver.FindElement(By.CssSelector("div[class='Umvnrc']")).Click();

            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            //wait until the keyboard is shown
            //wait.Until(ExpectedConditions.ElementIsVisible(By.Id("kbd")));
            wait.Until(d => d.FindElement(By.Id("kbd")));
            Driver.FindElement(By.Id("K81")).Click(); //this clicks on q key on keyboard
            wait.Until(d => d.FindElement(By.CssSelector("div[class='aajZCb']")));
            //Driver.FindElement(By.LinkText("Google-keresés")).Click();
            Driver.FindElement(By.Name("q")).SendKeys(Keys.Enter);


            Assert.AreEqual("q - Google-keresés", Driver.Title);
        }
    }
}