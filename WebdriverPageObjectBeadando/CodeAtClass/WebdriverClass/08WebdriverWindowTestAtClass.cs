﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Chrome;

using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;
using System.Threading;

namespace WebdriverClass
{
    class WebdriverWindowTestAtClass : TestBase
    {
        [Test]
        public void ResponsiveWindow()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            Driver.Navigate().GoToUrl("http://www.expedia.com/");
            Driver.Manage().Cookies.DeleteAllCookies();
            //Maximize browser window
            Driver.Manage().Window.Maximize();
            Driver.Navigate().Refresh();
            while(Driver.FindElements(By.CssSelector("div[class='listYourProperty__icon']")).Count==0)
            {
                Driver.Manage().Cookies.DeleteAllCookies();
                Driver.Navigate().Refresh();
            }
            wait.Until(d => d.FindElement(By.CssSelector("div[class='listYourProperty__icon']")));
            //Thread.Sleep(10000);
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            Assert.IsTrue(Driver.FindElement(By.CssSelector("div[class='listYourProperty__icon']")).Displayed);
            Assert.AreEqual(0,Driver.FindElements(By.CssSelector("svg[class='uitk-icon uitk-icon-leading uitk-icon-medium']")).Count);
            //Set browser window size to 600x600
            Driver.Manage().Window.Size = new Size(600,600);
            while (Driver.FindElements(By.CssSelector("svg[class='uitk-icon uitk-icon-leading uitk-icon-medium']")).Count == 0)
            {
                Driver.Manage().Cookies.DeleteAllCookies();
                Driver.Navigate().Refresh();
            }
            wait.Until(d => d.FindElement(By.CssSelector("svg[class='uitk-icon uitk-icon-leading uitk-icon-medium']")));
            //Thread.Sleep(5000);
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            Assert.IsTrue(Driver.FindElement(By.CssSelector("svg[class='uitk-icon uitk-icon-leading uitk-icon-medium']")).Displayed);
            Assert.AreEqual(0,Driver.FindElements(By.CssSelector("div[class='listYourProperty__icon']")).Count);
        }

        [Test]
        public void MultipleWindow()
        {
            // have to use Chrome because of SHIFT+Click, so Driver is overwritten
            Driver = new ChromeDriver();
            Driver.Navigate().GoToUrl("https://www.amazon.com/gp/gw/ajax/s.html");
            // String mainWindow <= save current window's handle in this string
            string mainWindow = Driver.CurrentWindowHandle;
            new Actions(Driver).KeyDown(Keys.Shift).Click(Driver.FindElement(By.CssSelector("a[href*='cart']"))).KeyUp(Keys.Shift).Perform();
            // ReadOnlyCollection<string> windows <= save all window handles here
            var windows = Driver.WindowHandles;
            // Switch to last opened window
            Driver.SwitchTo().Window(windows[windows.Count -1]);
            StringAssert.Contains("Cart", Driver.Title);
            // Close active window
            Driver.Close();
            // Switch to our main window
            Driver.SwitchTo().Window(mainWindow);
            Assert.IsTrue(Driver.FindElement(By.Id("gw-card-layout")).Displayed);
        }


    }
}
