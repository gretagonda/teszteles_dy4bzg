﻿using OpenQA.Selenium;

namespace WebdriverClass.PagesAtClass
{
    class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
