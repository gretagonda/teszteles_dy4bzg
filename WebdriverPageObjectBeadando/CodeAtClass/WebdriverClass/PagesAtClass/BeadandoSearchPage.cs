﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    class BeadandoSearchPage : BasePage
    {
        public BeadandoSearchPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static BeadandoSearchPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://ingatlan.com/";
            return new BeadandoSearchPage(webDriver);
        }

        public BeadandoPropertySearchWidget GetPropertySearchWidget()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            Driver.Navigate().Refresh();
            while ((Driver.Url == "https://error.ingatlan.com/") || (Driver.FindElements(By.CssSelector("img[src='https://ad.adverticum.net/banners/6664492/600x500_CORDIA_Q4_Interstitial.jpg']")).Count != 0) || (Driver.FindElements(By.CssSelector("div[class='interstitial overlay_2BPaU isOpened_2O0gr center_2ZsK3 middle_1RwfF']")).Count!=0))
            {
                Driver.Navigate().GoToUrl("https://ingatlan.com/");
                Driver.Navigate().Refresh();

            }
            wait.Until(d => d.FindElement(By.CssSelector("div[class='locations']")));
            return new BeadandoPropertySearchWidget(Driver);
        }

        public BeadandoAgentSearchWidget GetAgentSearchWidget()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            Driver.Navigate().Refresh();
            while ((Driver.Url == "https://error.ingatlan.com/") || (Driver.FindElements(By.CssSelector("img[src='https://ad.adverticum.net/banners/6664492/600x500_CORDIA_Q4_Interstitial.jpg']")).Count != 0) || (Driver.FindElements(By.CssSelector("div[class='interstitial overlay_2BPaU isOpened_2O0gr center_2ZsK3 middle_1RwfF']")).Count != 0))
            {
                Driver.Navigate().GoToUrl("https://ingatlan.com/");
                Driver.Navigate().Refresh();

            }

            wait.Until(d => d.FindElement(By.CssSelector("div[class='locations']")));
            return new BeadandoAgentSearchWidget(Driver);
        }
        public BeadandoPropertyResultWidget GetPropertyResultWidget()
        {
            return new BeadandoPropertyResultWidget(Driver);
        }


        public BeadandoAgentResultWidget GetAgentResultWidget()
        {
            return new BeadandoAgentResultWidget(Driver);
        }
    }
}
