﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.Models;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class BeadandoPropertySearchWidget : BasePage
    {
        public BeadandoPropertySearchWidget(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement SearchAcceptCookies => Driver.FindElement(By.Id("CybotCookiebotDialogBodyButtonAccept"));
        public IWebElement PropertySearchButton => Driver.FindElement(By.CssSelector("button[class='button']"));
        public IWebElement AgentSearchButton => Driver.FindElement(By.CssSelector("button[class='agent-searcher__submit js-agent-searcher-submit button--primary']"));
        public IWebElement LocationInput => Driver.FindElement(By.CssSelector("div[class='locations']"));
        public IWebElement LocationBox => Driver.FindElement(By.CssSelector("div[class='modalbox opened']"));
        public IWebElement InputFieldBox => Driver.FindElement(By.CssSelector("input[class='input-field']"));
        public IWebElement SuggestionsInBox => Driver.FindElement(By.CssSelector("div[class='suggestions']"));
        public IWebElement MinPrice => Driver.FindElements(By.CssSelector("input[placeholder='min']")).ToList()[0];
        public IWebElement MaxPrice => Driver.FindElements(By.CssSelector("input[placeholder='max']")).ToList()[0];
        public IWebElement MinArea => Driver.FindElements(By.CssSelector("input[placeholder='min']")).ToList()[1];
        public IWebElement MaxArea => Driver.FindElements(By.CssSelector("input[placeholder='max']")).ToList()[1];

        public void SetCity(string city)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            while ((Driver.Url == "https://error.ingatlan.com/") || (Driver.FindElements(By.CssSelector("img[src='https://ad.adverticum.net/banners/6664492/600x500_CORDIA_Q4_Interstitial.jpg']")).Count != 0) || (Driver.FindElements(By.CssSelector("div[class='interstitial overlay_2BPaU isOpened_2O0gr center_2ZsK3 middle_1RwfF']")).Count != 0))
            {
                Driver.Navigate().GoToUrl("https://ingatlan.com/");
                Driver.Navigate().Refresh();

            }
            wait.Until(d => d.FindElement(By.CssSelector("div[class='locations']")));
            LocationInput.Click();
            wait.Until(d => d.FindElement(By.CssSelector("div[class='modalbox opened']")));
            InputFieldBox.Click();
            InputFieldBox.SendKeys(city);
            wait.Until(d => d.FindElement(By.CssSelector("div[class='suggestions']")));
            InputFieldBox.SendKeys(Keys.Enter);
            InputFieldBox.SendKeys(Keys.Enter);


        }

        public void SetMinPrice(string price)
        {
            MinPrice.SendKeys(price);
        }


        public void SetMaxPrice(string price)
        {
            MaxPrice.SendKeys(price);
        }

        public void SetMinArea(string area)
        {
            MinArea.SendKeys(area);
        }

        public void SetMaxArea(string area)
        {
            MaxArea.SendKeys(area);
        }


        public BeadandoSearchPage SearchFor(PropertySearchModel search)
        {
            SetCity(search.city);
            SetMinPrice(search.minPrice);
            SetMaxPrice(search.maxPrice);
            SetMinArea(search.minArea);
            SetMaxArea(search.maxArea);
            return ClickPropertySearchButton();
        }


        public BeadandoSearchPage ClickPropertySearchButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            wait.Until(d => d.FindElement(By.CssSelector("button[class='button']")));
            PropertySearchButton.Click();

            wait.Until(d => d.FindElement(By.CssSelector("div[class='results__number']")));
            return new BeadandoSearchPage(Driver);
        }


    }
}
