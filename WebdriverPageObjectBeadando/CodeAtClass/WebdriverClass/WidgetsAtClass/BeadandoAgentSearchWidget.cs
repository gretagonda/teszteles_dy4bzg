﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class BeadandoAgentSearchWidget : BasePage
    {
        public BeadandoAgentSearchWidget(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement SearchAcceptCookies => Driver.FindElement(By.Id("CybotCookiebotDialogBodyButtonAccept"));
        public IWebElement AgentPageButton => Driver.FindElements(By.TagName("a")).First(element => element.Text == "Ingatlanos megbízása");

        public IWebElement AgentSearcherInput => Driver.FindElement(By.CssSelector("input[class='agent-searcher__input js-agent-searcher-input ui-autocomplete-input']"));


        public void SetAgentLocation(string agentLocation)
        {
            AgentSearcherInput.SendKeys(agentLocation);


        }

        public void NavigateToAgentPage()
        {
            AgentPageButton.Click();
        }

        public BeadandoSearchPage NavigateToAgents(string agentLocation)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            Driver.Manage().Window.Maximize();
            while ((Driver.Url == "https://error.ingatlan.com/") || (Driver.FindElements(By.CssSelector("img[src='https://ad.adverticum.net/banners/6664492/600x500_CORDIA_Q4_Interstitial.jpg']")).Count != 0) || (Driver.FindElements(By.CssSelector("div[class='interstitial overlay_2BPaU isOpened_2O0gr center_2ZsK3 middle_1RwfF']")).Count != 0))
            {
                Driver.Navigate().GoToUrl("https://ingatlan.com/");
                Driver.Navigate().Refresh();

            }
            wait.Until(d => d.FindElement(By.CssSelector("span[class='headerLinkContent']")));
            NavigateToAgentPage();
            wait.Until(d => d.FindElement(By.CssSelector("input[class='agent-searcher__input js-agent-searcher-input ui-autocomplete-input']")));
            SetAgentLocation(agentLocation);
            return ClickAgentSearchButton();

        }

        public BeadandoSearchPage ClickAgentSearchButton()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            AgentSearcherInput.SendKeys(Keys.Enter);

            wait.Until(d => d.FindElement(By.CssSelector("h1[class='search-header__headline']")));

            return new BeadandoSearchPage(Driver);
        }
    }
}

