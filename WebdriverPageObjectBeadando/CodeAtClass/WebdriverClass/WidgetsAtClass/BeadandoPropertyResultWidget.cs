﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class BeadandoPropertyResultWidget : BasePage
    {
        public BeadandoPropertyResultWidget(IWebDriver driver) : base(driver)
        {
        }
        private List<IWebElement> Listings => Driver.FindElements(By.CssSelector("div[class='listing js-listing ']")).ToList();
        private string resultPage => Driver.FindElement(By.CssSelector("h1[class='resultspage__location'")).Text;


        public int GetNoOfResults()
        {
            return Listings.Count;
        }

        public string GetResultHeader()
        {
            return resultPage;
        }
    }
}
