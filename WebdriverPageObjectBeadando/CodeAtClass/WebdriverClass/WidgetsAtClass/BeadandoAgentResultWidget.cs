﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class BeadandoAgentResultWidget : BasePage
    {
        public BeadandoAgentResultWidget(IWebDriver driver) : base(driver)
        {
        }

        private List<IWebElement> AgentList => Driver.FindElements(By.CssSelector("div[class='agent-block']")).ToList();
        private string resultPage => Driver.FindElement(By.CssSelector("h2[class='agent-list-header__title'")).Text;


        public int GetNumberOfAvaibleAgents()
        {
            return AgentList.Count;
        }

        public string GetResultHeader()
        {
            return resultPage;
        }
    }
}
