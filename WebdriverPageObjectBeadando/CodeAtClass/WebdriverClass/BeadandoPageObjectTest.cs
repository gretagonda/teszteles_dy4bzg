﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebdriverClass.Models;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("TestPropertyData")]
        public void PropertySearchWithDataDrivenTestingAndPageOpject(string city, string text, string minPrice, string maxPrice, string minArea, string maxArea)
        {
            PropertySearchModel searchModel = new PropertySearchModel();
            searchModel.city = city;
            searchModel.minPrice = minPrice;
            searchModel.maxPrice = maxPrice;
            searchModel.minArea = minArea;
            searchModel.maxArea = maxArea;


            int numberOfResults = BeadandoSearchPage.Navigate(Driver)
                .GetPropertySearchWidget()
                .SearchFor(searchModel)
                .GetPropertyResultWidget()
                .GetNoOfResults();

            string resultHeader = BeadandoSearchPage.Navigate(Driver)
                .GetPropertySearchWidget()
                .SearchFor(searchModel)
                .GetPropertyResultWidget()
                .GetResultHeader();

            Assert.Greater(numberOfResults, 0);
            Assert.True(text.Equals(resultHeader.Trim()));
        }

        [Test, TestCaseSource("TestAgentData")]
        public void AgentLocationSearch(string agentLocation)
        {
            int numberOfAvaibleAgents = BeadandoSearchPage.Navigate(Driver)
                .GetAgentSearchWidget()
                .NavigateToAgents(agentLocation)
                .GetAgentResultWidget()
                .GetNumberOfAvaibleAgents();

            string headerOfAgentPages = BeadandoSearchPage.Navigate(Driver)
                .GetAgentSearchWidget()
                .NavigateToAgents(agentLocation)
                .GetAgentResultWidget()
                .GetResultHeader();

            Assert.Greater(numberOfAvaibleAgents, 0);
            StringAssert.StartsWith("Hitelesített", headerOfAgentPages);
        }

        static IEnumerable TestPropertyData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\propertysearch.xml");
            return
                from vars in doc.Descendants("cityData")
                let city = vars.Attribute("city").Value
                let text = vars.Attribute("text").Value
                let minPrice = vars.Attribute("minPrice").Value
                let maxPrice = vars.Attribute("maxPrice").Value
                let minArea = vars.Attribute("minArea").Value
                let maxArea = vars.Attribute("maxArea").Value
                select new object[] { city, text, minPrice, maxPrice, minArea, maxArea };
        }


        static IEnumerable TestAgentData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\agentsearch.xml");
            return
                from vars in doc.Descendants("agentData")
                let agentLocation = vars.Attribute("agentLocation").Value
                select new object[] { agentLocation };
        }
    }

    
}
